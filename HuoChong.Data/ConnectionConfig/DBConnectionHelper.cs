﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace HuoChong.Data.ConnectionConfig
{
  public  class DBConnectionHelper
    {
        public static  string ConnectionStr{ get; set; }
        public static IDbConnection OpenConnection()
        {
            if (string.IsNullOrEmpty(ConnectionStr))
            {
                throw new ArgumentNullException(nameof(ConnectionStr));
            }
            IDbConnection conn = new SqlConnection(ConnectionStr);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            return conn;
           
        }
    }
}
