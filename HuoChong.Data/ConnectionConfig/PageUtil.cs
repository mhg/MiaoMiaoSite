﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Data.ConnectionConfig
{
    public class PageUtil
    {   /// <summary>
        /// 分页语句
        /// </summary>
        public static string Pagination = "OFFSET (@PageIndex-1) * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY";
    }
}
