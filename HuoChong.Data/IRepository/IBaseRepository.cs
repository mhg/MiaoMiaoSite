﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HuoChong.Data.IRepository
{
    /// <summary>
    /// 基接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBaseRepository<T> where T : class, new()
    {
        #region 同步方法
        IList<T> GetList();
        IList<T> GetList(string strWhere, object parameters = null);
        T GetModel(int id = 1, string name = null);
        IList<T> GetListPage(int pageNum, int rowsNum, string strWhere=null, string orderBy=null, object parameters=null);
        int Delete(T entity);
        bool Insert(T entity);
        int Update(T entity);
        int DeleteList(string strWhere, object parameters = null);
        int RecordCount(string strWhere = null, object parameters = null);
        #endregion
        #region 异步方法
        Task<int> UpdateAsync(T entity);
        Task<bool> InsertAsync(T entity);
        Task<int> DeleteAsync(T entity);
        Task<int> DeleteListAsync(string strWhere, object parameters = null);
        Task<IList<T>> GetListAsync();
        Task<T> GetModelAsync(int id);
        Task<IList<T>> GetListAsync(string strWhere, object parameters = null);
        Task<IList<T>> GetListPageAsync(int pageNum, int rowsNum, string strWhere = null, string orderBy = null, Object parameters = null);
        Task<int> RecordCountAsync(string strWhere = null, object parameters = null);
        #endregion

    }
}
