﻿using HuoChong.Entity.ViewModel;
using HuoChong.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HuoChong.Data.IRepository
{
    public interface IColumnRepository:IBaseRepository<Column>
    {
        int Del(string ids);
        Task<Tuple<List<ColumnVM>,int>> LoadColumnPageAsync(int pageIndex, int pageSize, string key);
        List<ColumnVM> GetChildcolumnByParentColumn(string columnParentSpell);
        List<ColumnVM> GetAllChildcolumn();
    }
}
