﻿using HuoChong.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Data.IRepository
{
    public  interface IKeyWordRepository: IBaseRepository<KeyWord>
    {
    }
}
