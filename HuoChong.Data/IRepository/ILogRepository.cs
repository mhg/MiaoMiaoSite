﻿using HuoChong.Entity;
using HuoChong.Entity.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Data.IRepository
{
    public interface ILogRepository:IBaseRepository<Log>
    {
        Tuple<IList<LogVM>,int> LoadLog(int pageIndex, int pageSize, string key);
        int MoreDelLog(string ids);
    }
}
