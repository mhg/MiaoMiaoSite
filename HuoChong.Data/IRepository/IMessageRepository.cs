﻿using HuoChong.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Data.IRepository
{
  public  interface IMessageRepository: IBaseRepository<Message>
    {
        List<Message> GetMessageList(int newsId);
    }
}
