﻿using HuoChong.Entity;
using HuoChong.Entity.ViewModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HuoChong.Data.IRepository
{
    public interface INewsRepository : IBaseRepository<News>
    {
        Task<Tuple<IList<NewsPageVM>, int>> LoadNewsPage(int pageIndex, int pageSize, string columnParentSpell);
        Task<Tuple<IList<NewsPageVM>, int>> LoadNewsPage(int pageIndex, int pageSize, string key, int? check, int? rec, int? parentId = null, int? childId = null, bool isFrontweb = false);
        /// <summary>
        /// 首页推荐
        /// </summary>     
        Task<IList<News>> LoadIndexRecNews(int top);
        Task<NewsPreNextVM> LoadPreNextNews(int id, string columnUrl, string type);
        Task<IList<NewsColumnRealtedVM>> LoadNewsColumnRelated(int columnId, int top);
        Task<News> NewsDetailAsync(int id);

        Task<IList<BrowseNumNewsVm>> GetByBrowseNewsAsync(string type);
        Task<IList<BrowseNumNewsVm>> GetByLikeNewsAsync(string type);
        Task<bool> UpdateNews(News input);
        Task<bool> DelNews(string ids);
        Task<bool> IsCheckNews(string ids, bool isCheck);
        Task<bool> IsRecNews(string ids, bool isRec);
        Task<IList<NewsPageVM>> LoadCategoryNews();
        List<IndexNewsVM> GetNewNewsByParentColumn(int top, string parentColunName);
        List<IndexNewsVM> RandomNews();

    }
}
