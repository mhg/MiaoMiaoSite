﻿using HuoChong.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Data.IRepository
{
   public interface IPermissionRepository : IBaseRepository<Permission>
    {
        int UpdateTemp(int adminId, int menuId, bool isPass);
        Permission GetModel(int adminId, int menuId);
        int RealyDelTemp(int adminId, int menuId);
    }
}
