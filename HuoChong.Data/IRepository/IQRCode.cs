﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace HuoChong.Data.IRepository
{
    public interface IQRCode
    {
        Bitmap GetQRCode(string url, int pixel);
    }
}
