﻿using HuoChong.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Data.IRepository
{
  public  interface ISpliderSiteRepository:IBaseRepository<SpliderSite>
    {
        int UpdateSite(int id);
        int MoreDelSite(string ids);
    }
}
