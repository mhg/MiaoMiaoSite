﻿using HuoChong.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HuoChong.Data.IRepository
{
    public interface IUserRepository:IBaseRepository<User>
    {
        Task<User> Login(string userName);
        int MoreDelUser(string ids);
    }
}
