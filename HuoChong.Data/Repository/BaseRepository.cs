﻿using Dapper;
using HuoChong.Data.ConnectionConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuoChong.Data.Repository
{
    public class BaseRepository<T> where T : class, new()
    {
        #region 同步方法
        public int Delete(T entity)
        {
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return conn.Delete(entity);
            }
        }
        public int DeleteList(string strWhere, object parameters)
        {
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return conn.DeleteList<int>(strWhere, parameters);
            }
        }
        public IList<T> GetList()
        {
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return conn.GetList<T>().ToList();
            }

        }

        public IList<T> GetListPage(int pageNum, int rowsNum, string strWhere=null, string orderBy=null, object parameters=null)
        {
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return conn.GetListPaged<T>(pageNum, rowsNum, strWhere, orderBy, parameters).ToList();
            }
        }

        public T GetModel(int id = 1, string name = null)
        {
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return conn.Get<T>(id);
            }
        }

        public IList<T> GetList(string strWhere, object parameters = null)
        {
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return conn.GetList<T>(strWhere, parameters).ToList();
            }
        }

        public bool Insert(T entity)
        {
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return conn.Insert<T>(entity) > 0;
            }
        }
        public int Update(T entity)
        {
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return conn.Update(entity);
            }
        }
        public int RecordCount(string strWhere = null, object parameters = null)
        {
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return  conn.RecordCount<T>(strWhere, parameters);
            }
        }
        #endregion
        #region 异步方法
        public async Task<int> UpdateAsync(T entity)
        {

            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return await conn.UpdateAsync(entity);
            }
        }

        public async Task<bool> InsertAsync(T entity)
        {
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return await conn.InsertAsync<T>(entity) > 0;
            }
        }
        public async Task<int> DeleteAsync(T entity)
        {
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return await conn.DeleteAsync(entity);
            }
        }
        public async Task<int> DeleteListAsync(string strWhere, object parameters = null)
        {
            using (var conn = DBConnectionHelper.OpenConnection())
            {

                return await conn.DeleteListAsync<T>(strWhere, parameters);

            }
        }
        public async Task<IList<T>> GetListAsync()
        {
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return (await conn.GetListAsync<T>()).ToList();
            }
        }

        public async Task<T> GetModelAsync(int id)
        {
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return await conn.GetAsync<T>(id);
            }
        }
        public async Task<IList<T>> GetListAsync(string strWhere, object parameters = null)
        {
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return (await conn.GetListAsync<T>(strWhere, parameters)).ToList();
            }
        }
        public async Task<IList<T>> GetListPageAsync(int pageNum, int rowsNum, string strWhere = null, string orderBy = null, object parameters = null)
        {
            using (var conn = DBConnectionHelper.OpenConnection())
            {

                return (await conn.GetListPagedAsync<T>(pageNum, rowsNum, strWhere, orderBy, parameters)).ToList();
            }
        }
        /// <summary>
        /// 查询总条数条数
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async Task<int> RecordCountAsync(string strWhere = null, object parameters = null)
        {
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return await conn.RecordCountAsync<T>(strWhere, parameters);
            }
        }
        #endregion
    }
}
