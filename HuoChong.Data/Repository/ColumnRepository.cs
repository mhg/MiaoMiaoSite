﻿using Dapper;
using HuoChong.Entity.ViewModel;
using HuoChong.Data.ConnectionConfig;
using HuoChong.Data.IRepository;
using HuoChong.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuoChong.Data.Repository
{
   public class ColumnRepository:BaseRepository<Column>, IColumnRepository
    {
        public int Del(string ids)
        {
            string sql = "delete from [column] where id in @Ids";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return conn.Execute(sql, new { Ids = ids.Split(',') });
            }
        }
        public async Task<Tuple<List<ColumnVM>, int>> LoadColumnPageAsync(int pageIndex, int pageSize, string key)
        {
          StringBuilder sqlQuery=new StringBuilder(@"
                            SELECT NAME      AS ChildName,
                                   Id,
                                   Spell,
                                   CreateTime,
                                   parentid,
                                   (
                                       SELECT NAME
                                       FROM   [column] AS c2
                                       WHERE  c2.id = c1.parentid
                                   )         AS ColumnParentName
                            FROM   [column]     c1
                            ");
            DynamicParameters dp = new DynamicParameters();
            dp.Add("PageIndex", pageIndex);
            dp.Add("PageSize", pageSize);
          
            StringBuilder countQuery = new StringBuilder($@"select  count(1)  from  [column] c1");
            DynamicParameters dp2 = new DynamicParameters();
            if (!string.IsNullOrEmpty(key))
            {
                sqlQuery.Append(" where c1.Name=@Key ");
                dp.Add("Key", key);
                countQuery.Append(" where c1.Name=@Key ");
                dp2.Add("Key", key);
            }
            sqlQuery.Append($@" order  by c1.CreateTime  desc  {PageUtil.Pagination}");
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                var query = (await conn.QueryAsync<ColumnVM>(sqlQuery.ToString(), dp)).ToList();
                var total = await conn.ExecuteScalarAsync<int>(countQuery.ToString(), dp);
                return new Tuple<List<ColumnVM>, int>(query, total);
            }
        }
        public List<ColumnVM> GetChildcolumnByParentColumn(string columnParentSpell)
        {
            string sql = @"select c.Spell,c.Name as ChildName  from  [column] c ";
            //where c.ParentId in(select Id  from [Column] cc where cc.Spell=@ParentColumn )
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return  conn.Query<ColumnVM>(sql,new { ParentColumn = columnParentSpell }).ToList();
            }
        }
        public List<ColumnVM> GetAllChildcolumn()
        {
            string sql = @"select  c.Spell ,c.Name as ChildName  from  [column] c where c.ParentId<>0";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return conn.Query<ColumnVM>(sql).ToList();
            }
        }

    }
}
