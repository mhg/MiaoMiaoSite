﻿using HuoChong.Data.ConnectionConfig;
using HuoChong.Data.IRepository;
using HuoChong.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using Dapper;
using HuoChong.Entity.ViewModel;
using System.Linq;

namespace HuoChong.Data.Repository
{
   public class LogRepository:BaseRepository<Log>, ILogRepository
    {
        public int MoreDelLog(string ids)
        {
            string sql = $"delete from log where id in@Ids";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return conn.Execute(sql,new { Ids=ids.Split(',')});
            }
        }
      public  Tuple<IList<LogVM>, int> LoadLog(int pageIndex, int pageSize, string key)
        {
            StringBuilder sqlQuery = new StringBuilder($@"select g.id,g.userid,g.ip,g.content,g.createtime,g.address, u.userName from  [log] g inner  join   [user] as u  on g.userId=u.id where u.isdel={(short)UserIsDel.No}");
            DynamicParameters dp = new DynamicParameters();
            dp.Add("PageIndex", pageIndex);
            dp.Add("PageSize", pageSize);
            DynamicParameters dp2 = new DynamicParameters();
           

            StringBuilder countQuery = new StringBuilder($@"select  count(1)  from [log] g inner  join   [user] as u  on g.userId=u.id where u.isdel={(short)UserIsDel.No}");
            if (!string.IsNullOrEmpty(key))
            {
                sqlQuery.Append(" and u.userName=@Key ");
                dp.Add("Key", key);
                countQuery.Append(" and u.userName=@Key ");
                dp2.Add("Key", key);
            }
            sqlQuery.Append($@" order  by g.CreateTime  desc  {PageUtil.Pagination}");
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                var query =  conn.Query<LogVM>(sqlQuery.ToString(), dp).ToList();
                var total = conn.ExecuteScalar<int>(countQuery.ToString());
                return new Tuple<IList<LogVM>, int>(query, total);
            }
        }
    }
}
