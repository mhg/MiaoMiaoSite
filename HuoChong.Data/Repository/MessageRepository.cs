﻿using Dapper;
using HuoChong.Data.IRepository;
using HuoChong.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HuoChong.Data.Repository
{
   public class MessageRepository: BaseRepository<Message>, IMessageRepository
    {
        public List<Message> GetMessageList(int newsId)
        {
            string sql = "select *  from message where newsid=@NewsId";
            using (var conn=ConnectionConfig.DBConnectionHelper.OpenConnection())
            {
                return conn.Query<Message>(sql, new { NewsId = newsId }).ToList();
            }
        }
    }
}
