﻿using Dapper;
using HuoChong.Data.ConnectionConfig;
using HuoChong.Data.IRepository;
using HuoChong.Entity;
using HuoChong.Entity.ViewModel;
using HuoChong.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuoChong.Data.Repository
{
    public class NewsRepository : BaseRepository<News>, INewsRepository
    {
        #region 网站前台分类新闻加载
        /// <summary>
        /// 网站前台分类新闻加载
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="columnParentSpell">栏目名称拼音</param>
        /// <returns></returns>
        public async Task<Tuple<IList<NewsPageVM>, int>> LoadNewsPage(int pageIndex, int pageSize, string columnParentSpell)
        {
            StringBuilder sqlQuery = new StringBuilder($@"select  
                                             n.Title ,
                                             n.Content,
                                             n.[source],
                                             n.Province,                                           
                                             c.Name as ColumnName,
                                             c.Id as ColumnId,
                                              (select cc.Spell from [Column] cc where cc.Id=n.columnid) as ColumnUrl ,
                                             n.CreateTime,
                                             n.Id,
                                             n.Author, 
                                             n.Intros,
                                             ( select count(1) from  BrowseNum b where n.Id=b.newsId)as [views],
                                             ( select count(1) from  [like] l where l.newsId=n.id)as [like]
                                      from  News  n inner  join   [Column] c  on  n.columnId=c.id or n.ColumnChildId=c.Id
                                      where n.IsCheck={(short)NewsIsCheck.Yes} and c.Spell='{columnParentSpell}'");
            DynamicParameters dp = new DynamicParameters();
            dp.Add("PageIndex", pageIndex);
            dp.Add("PageSize", pageSize);
            sqlQuery.Append($@" order  by n.CreateTime  desc  {PageUtil.Pagination}");

            string countQuery = $@"select  count(1)  from  News  n inner  join   [Column] c  on  n.columnId=c.id or n.ColumnChildId=c.Id
                                where n.ischeck={(short)NewsIsCheck.Yes} and c.Spell='{columnParentSpell}'";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                var query = (await conn.QueryAsync<NewsPageVM>(sqlQuery.ToString(), dp)).ToList();
                var total = await conn.ExecuteScalarAsync<int>(countQuery);
                return new Tuple<IList<NewsPageVM>, int>(query, total);
            }
        }
        #endregion

        #region 后台新闻列表
        public async Task<Tuple<IList<NewsPageVM>, int>> LoadNewsPage(int pageIndex, int pageSize, string key,
         int? check, int? rec, int? parentId = null, int? childId = null,bool isFrontweb=false)
        {
            try
            {
                StringBuilder sqlQuery = new StringBuilder($@"select  
                                             n.Title ,
                                             n.[source],
                                             n.Province,                                           
                                             c.Name as ColumnName,
                                             c.Id as ColumnId,
                                             n.CreateTime,
                                             n.Id,
                                             n.Author, 
                                             n.Intros,
                                             n.IsCheck,
                                             n.IsRec,
                                             n.IsOrigin,
                                             n.Content,
                                             c.Spell as ColumnUrl,
                                             ( select count(1) from  BrowseNum b where n.Id=b.newsId)as [views],
                                             ( select count(1) from  [like] l where l.newsId=n.id)as [like]
                                      from  News  n inner  join   [Column] c  on  n.columnId=c.id
                                      where n.IsCheck={check}");

                StringBuilder countQuery = new StringBuilder($@"select  count(1)  from  News  n inner  join   [Column] c  on    n.columnId=c.id  where n.ischeck={check}");

                DynamicParameters dp = new DynamicParameters();
                dp.Add("PageIndex", pageIndex);
                dp.Add("PageSize", pageSize);
                DynamicParameters dp2 = new DynamicParameters();
                if (!string.IsNullOrEmpty(key))
                {
                    if (isFrontweb)
                    {
                        sqlQuery.Append(" and n.KeyWord like '%'+@Key+'%' ");
                        dp.Add("Key", key);
                        countQuery.Append("  and n.KeyWord like '%'+@Key+'%' ");
                        dp2.Add("Key", key);
                    }
                    else
                    {
                        sqlQuery.Append(" and n.Title=@Key ");
                        dp.Add("Key", key);
                        countQuery.Append(" and n.Title=@Key ");
                        dp2.Add("Key", key);
                    }
                }                                               
                if (rec.HasValue)
                {
                    sqlQuery.Append(" and n.IsRec=@IsRec ");
                    dp.Add("IsRec", rec);
                    countQuery.Append(" and n.IsRec=@IsRec ");
                    dp2.Add("IsRec", rec);
                }
                if (parentId.HasValue)
                {
                    sqlQuery.Append(" and n.ColumnId=@ColumnId ");
                    dp.Add("ColumnId", parentId);
                    countQuery.Append(" and n.ColumnId=@ColumnId ");
                    dp2.Add("ColumnId", parentId);
                }
                if (childId.HasValue)
                {
                    sqlQuery.Append(" and n.ColumnChildId=@ColumnChildId");
                    dp.Add("ColumnChildId", childId);
                    countQuery.Append(" and n.ColumnChildId=@ColumnChildId");
                    dp2.Add("ColumnChildId", childId);
                }
                sqlQuery.Append($@" order  by n.CreateTime  desc  {PageUtil.Pagination}");
                using (var conn = DBConnectionHelper.OpenConnection())
                {
                    var query = (await conn.QueryAsync<NewsPageVM>(sqlQuery.ToString(), dp)).ToList();
                    var total = await conn.ExecuteScalarAsync<int>(countQuery.ToString(), dp);
                    return new Tuple<IList<NewsPageVM>, int>(query, total);
                }
            }
            catch (Exception ex)
            {
                NLogHelp.ErrorLog(ex.Message, ex);
                throw;
            }
        }
        #endregion
        #region 前台加载
        public async Task<IList<NewsPageVM>> LoadCategoryNews()
        {
            string sql = $@"select top 12
                                      ROW_NUMBER() over(order by n.CreateTime desc) as rows,
                                      n.Title,
                                      n.Content,
                                      n.[source],                                    
                                      c.Name as ColumnName,
                                      c.Id as ColumnId,
                                      c.Spell as ColumnUrl,
                                      n.CreateTime,
                                      n.Id,
                                      n.Author,
                                      n.Intros,
                                      (select count(1) from BrowseNum b where n.Id = b.newsId) as [views],
                                      (select count(1) from[like] l where l.newsId = n.id)as [like]
                                      from News n inner
                                      join   [Column] c  on n.columnId = c.id
                                      where n.IsCheck ={(short)NewsIsCheck.Yes}";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return (await conn.QueryAsync<NewsPageVM>(sql)).ToList();

            }

        }
        #endregion
        #region 首页推荐新闻加载
        public async Task<IList<News>> LoadIndexRecNews(int top)
        {
            string sql = $@"select top {top} 
                                    n.Title ,
                                    n.Content,                                                               
                                    n.ColumnId,
                                    n.CreateTime,
                                    n.Id,
                                    n.Intros ,
                                    (select spell from [Column] c where c.id=n.columnId)as ColumnUrl
                            from news n
                          where n.IsRec={(short)NewsIsRec.Yes}and n.IsCheck={(short)NewsIsCheck.Yes}
                          order by updateTime desc";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                var query = (await conn.QueryAsync<News>(sql)).ToList();
                return query;
            }
        }
        #endregion
        #region 新闻上下篇
        public async Task<NewsPreNextVM> LoadPreNextNews(int id, string columnUrl, string type)
        {
            string sql = $@"select top 1 n.title,n.id from news n  inner join [column] c  on n.columnid=c.id where isCheck={(short)NewsIsCheck.Yes}";
            if (type == "pre")
            {
                sql += " and n.id<@Id and c.spell=@ColumnUrl  order by  id desc";
            }
            if (type == "next")
            {
                sql += " and n.id>@Id and c.spell=@ColumnUrl order by id asc";
            }
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return await conn.QueryFirstOrDefaultAsync<NewsPreNextVM>(sql, new { Id = id, ColumnUrl = columnUrl });
            }
        }
        #endregion
        public async Task<IList<NewsColumnRealtedVM>> LoadNewsColumnRelated(int columnId, int top)
        {
            string sql = $@"select top {top} title ,id  from news 
                            where columnId={columnId}and isCheck={(short)NewsIsCheck.Yes}
                            order by createTime  desc";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return (await conn.QueryAsync<NewsColumnRealtedVM>(sql)).ToList();
            }
        }
        #region 新闻详情
        public async Task<News> NewsDetailAsync(int id)
        {
            string sql = $@"select top 1
                                          n.Title ,
                                          n.Content,
                                          n.source,
                                          n.Province,
                                          c.Name as ColumnName,
                                          c.Id as ColumnId,
                                          n.ColumnChildId,
                                          c.Spell as ColumnUrl,
                                          n.CreateTime,
                                          n.Id,
                                          n.Author, 
                                          n.Intros,
                                          n.KeyWord,
                                          n.IsOrigin,
                                          (select count(1) from BrowseNum b where n.Id = b.newsId)as views,
                                          ( select count(1) from  [like] l where n.Id=l.newsId)as [like]
                                      from News n inner
                                      join   [Column] c  on n.columnId = c.id
                                      where n.IsCheck ={ (short)NewsIsCheck.Yes} and n.id=@Id";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return await conn.QueryFirstOrDefaultAsync<News>(sql, new { Id = id });
            }
        }
        #endregion

        #region 浏览量和点赞新闻加载
        public async Task<IList<BrowseNumNewsVm>> GetByBrowseNewsAsync(string type)
        {
            string sql = $@"select top 8 
                                   n.Title ,
                                   n.Content,
                                   n.Id,
                                   n.Intros,
                                    (select spell from [Column] c where c.id=n.columnId)as ColumnUrl,
                                   ( select count(1) from  BrowseNum b where n.Id=b.newsId)as [views] 
                                 from  News  n 
                                 where n.IsCheck={(short)NewsIsCheck.Yes}  and datediff({type},CreateTime,getdate())=0
								 order  by  [views] desc ";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return (await conn.QueryAsync<BrowseNumNewsVm>(sql)).ToList();
            }
        }

        public async Task<IList<BrowseNumNewsVm>> GetByLikeNewsAsync(string type)
        {
            string sql = $@"select top 10 
                                   n.Title ,
                                   n.Content,
                                   n.Id,
                                   n.Intros,
                                    (select spell from [Column] c where c.id=n.columnId)as ColumnUrl,
                                   ( select count(1) from  [like] l where n.Id=l.newsId)as [like] 
                                 from  News  n 
                                 where n.IsCheck={(short)NewsIsCheck.Yes} and n.Isrec={(short)NewsIsRec.Yes} and datediff({type},CreateTime,getdate())=0
								 order  by  [like] desc ";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return (await conn.QueryAsync<BrowseNumNewsVm>(sql)).ToList();
            }
        }
        public List<IndexNewsVM> GetNewNewsByParentColumn(int top,string parentColunName)
        {
            string sql = $@"select  top {top} Title,Id ,(select  c.Spell  from [Column] c where c.Name='{parentColunName}')as ColumnUrl from  News  n where n.ColumnId=(select  Id  from [Column] c where c.Name='{parentColunName}')and n.IsCheck=1   order by n.CreateTime desc";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return  conn.Query<IndexNewsVM>(sql).ToList();
            }
        }
        public List<IndexNewsVM> RandomNews()
        {
            string sql = $@"select top 10 n.Title,n.Id,c.Spell as ColumnUrl  from  News n inner join [column] c on n.ColumnId=c.Id  where IsCheck=1 order by NEWID()";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return conn.Query<IndexNewsVM>(sql).ToList();
            }
        }
        #endregion

        #region 后台新闻 更新  删除 审核 推荐
        public async Task<bool> UpdateNews(News input)
        {
            string sql = @"UPDATE [dbo].[News]
                           SET [Title] =@Title,
                              [Intros] =@Intros,
                              [Content] = @Content,
                              [Source] = @Source, 
                              [Author] = @Author,
                              [UpdateTime] =@UpdateTime,
                              [ColumnId] =@ColumnId,
                              [Province] = @Province,
                              [IsCheck] = @IsCheck,
                              [KeyWord] = @KeyWord,
                              [ColumnChildId]=@ColumnChildId,
                              [IsRec]=@IsRec,
                              [CreateTime]=@CreateTime,
                              [IsOrigin]=@IsOrigin
                         WHERE id=@Id";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return await conn.ExecuteAsync(sql, input) > 0;
            }
        }

        public async Task<bool> DelNews(string ids)
        {
            string sql = "delete from news where id in @Ids";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return await conn.ExecuteAsync(sql, new { Ids = ids.Split(',') }) > 0;
            }
        }
        public async Task<bool> IsCheckNews(string ids, bool isCheck)
        {
            string sql = "update [dbo].[News] set [IsCheck] = @IsCheck where id in @Ids";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return await conn.ExecuteAsync(sql, new { Ids = ids.Split(','), IsCheck = isCheck }) > 0;
            }
        }
        public async Task<bool> IsRecNews(string ids, bool isRec)
        {
            string sql = "update [dbo].[News] set [IsRec] = @IsRec where id in @Ids";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return await conn.ExecuteAsync(sql, new { Ids = ids.Split(','), IsRec = isRec }) > 0;
            }
        }
        #endregion
        #region MyRegion
       
        #endregion
    }
}
