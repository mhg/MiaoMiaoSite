﻿using Dapper;
using HuoChong.Data.ConnectionConfig;
using HuoChong.Data.IRepository;
using HuoChong.Entity;

namespace HuoChong.Data.Repository
{
    public class PermissionRepository : BaseRepository<Permission>, IPermissionRepository
    {
        public int UpdateTemp(int adminId, int menuId, bool isPass)
        {
            string sql = "update Permission set ispass=@IsPass  where  UserId=@UserId  and MenuId=@MenuId ";

            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return conn.Execute(sql, new { IsPass = isPass, UserId = adminId, MenuId = menuId });
            }
        }
        public Permission GetModel(int adminId, int menuId)
        {
            string sql = "select  top 1 *  from Permission   where  UserId=@UserId  and MenuId=@MenuId ";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return conn.QueryFirstOrDefault<Permission>(sql, new { UserId = adminId, MenuId = menuId });
            }
        }
        public int RealyDelTemp(int adminId, int menuId)
        {
            string sql = "  delete from Permission where UserId=@UserId and MenuId=@MenuId ";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return  conn.Execute(sql, new { UserId = adminId, MenuId = menuId });
            }

        }
    }
}
