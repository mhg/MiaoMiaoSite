﻿using Dapper;
using HuoChong.Data.ConnectionConfig;
using HuoChong.Data.IRepository;
using HuoChong.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Data.Repository
{
  public  class SpliderSiteRepository: BaseRepository<SpliderSite>,ISpliderSiteRepository
    {
        public int UpdateSite(int id)
        {
            string sql = "update SpliderSite set IsSplider=1 where id=@Id ";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return conn.Execute(sql, new { Id=id});
            }
        }
        public int MoreDelSite(string ids)
        {
            string sql = "delete from SpliderSite  where id in @Ids ";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return conn.Execute(sql, new { Ids = ids.Split(',') });
            }
        }
    }
}
