﻿using Dapper;
using HuoChong.Data.ConnectionConfig;
using HuoChong.Data.IRepository;
using HuoChong.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HuoChong.Data.Repository
{
   public class UserRepository: BaseRepository<User>,IUserRepository
    {
        public async Task<User> Login(string userName)
        {
            string sql = $"select top 1 * from [user] where isDel={(short)UserIsDel.No} and UserName=@UserName";
            using (var conn = DBConnectionHelper.OpenConnection())
            {
                return await conn.QueryFirstOrDefaultAsync<User>(sql, new { UserName = userName });
            }
        }
        public int MoreDelUser(string ids)
        {
            string sql = $@"update [user] set isdel={(short)UserIsDel.Yes} where id in @Ids";
            using (var conn=DBConnectionHelper.OpenConnection())
            {
                return conn.Execute(sql,new {Ids=ids.Split(',') });
            }
        }
    }
}
