﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Entity.AppSettings
{
    public class DomainConfig
    {
        /// <summary>
        /// 前台域名
        /// </summary>
        public string Host { get; set; }
        /// <summary>
        /// 图片域名
        /// </summary>
        public string  ImgHost { get; set; }
    }
}
