﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Entity.AppSettings
{
   public class MailConfig
    {
        public string Name { get; set; }
        public string Host { get; set; }
        public string Address { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
    }
}
