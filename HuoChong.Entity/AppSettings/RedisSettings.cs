﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Entity.AppSettings
{
  public  class RedisSettings
    {
        public string AccessKey { get; set; }

        public string SecretKey { get; set; }

        public string Bucket { get; set; }

        public string Domain { get; set; }
    }
}
