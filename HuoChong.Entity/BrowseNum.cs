﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuoChong.Entity
{
    [Table("BrowseNum")]
    public class BrowseNum
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreateTime { get; set; } = DateTime.Now;
        public int NewsId { get; set; }
        public string IP { get; set; }
    }
}
