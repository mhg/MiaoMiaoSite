﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HuoChong.Entity
{
    /// <summary>
    /// 标签（栏目）
    /// </summary>
    [Table("Column")]
   public class Column
    {
        [Key]
        public int Id { get; set; }
        public string  Name { get; set; }
        public DateTime CreateTime { get; set; }=DateTime.Now;
        public int ParentId { get; set; }
        public string Spell { get; set; }
        public string Title { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }
    }
}
