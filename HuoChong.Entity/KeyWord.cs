﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HuoChong.Entity
{
    [Table("KeyWord")]
    public  class KeyWord
    {
        [Key]
        public int Id { get; set; }
        public string KeyName { get; set; }
        public string Url { get; set; }     
        public int Ranking { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }
    }
}
