﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HuoChong.Entity
{
    [Table("Like")]
   public class Like
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreateTime { get; set; }
        public int NewsId { get; set; }
        public string IP { get; set; }
    }
}
