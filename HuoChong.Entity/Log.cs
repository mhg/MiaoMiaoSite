﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HuoChong.Entity
{
    [Table("Log")]
    public  class Log
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime CreateTime { get; set; }
        public string  Content { get; set; }
        public string IP  { get; set; }
        public string  Address { get; set; }
    }
}
