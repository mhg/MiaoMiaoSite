﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HuoChong.Entity
{
    [Table("KeyWord")]
    public  class Menu
    { [Key]
        public int Id { get; set; }

        public string MenuUrl { get; set; }

        public string MenuName { get; set; }
        /// <summary>
        /// 权限图标
        /// </summary>
        public string MenuImg { get; set; }
        public DateTime? CreateTime { get; set; }
   
        /// <summary>
        /// 权限父id
        /// </summary>
        public int ParentId { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int? OrderBy { get; set; }
        /// <summary>
        /// true为删除，false为正常状态
        /// </summary>
        public bool IsDelete { get; set; }
        /// <summary>
        /// 权限类型（0：普通权限，1:菜单权限）
        /// </summary>
        public int? MenuType { get; set; }
        /// <summary>
        /// get,post
        /// </summary>
        public string HttpMethod { get; set; }
    }
}
