﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Entity
{
  public  class Message
    {
        public int Id { get; set; }
        public string  Content { get; set; }
        public DateTime CreateTime { get; set; }
        public string IP { get; set; }
        public string Address { get; set; }
        public bool IsReply { get; set; }
        public int ParentId { get; set; }
        public string Email { get; set; }
        public int NewsId { get; set; }
    }
}
