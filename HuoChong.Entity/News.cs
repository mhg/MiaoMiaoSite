﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HuoChong.Entity
{
    [Table("News")]
    public class News
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Source { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }
        public string Author { get; set; }
        public int ColumnId { get; set; }
        public int ColumnChildId { get; set; }
        public string Province { get; set; }
        [NotMapped]
        public int Views { get; set; }
        public string Intros { get; set; }
        public string  KeyWord { get; set; }
        [NotMapped]
        public int Like { get; set; }
        [NotMapped]
        public string  ColumnUrl { get; set; }
        [NotMapped]
        public string ColumnName { get; set; }
        public NewsIsCheck IsCheck { get; set; }
        public NewsIsRec IsRec { get; set; }
        public NewsIsOrigin IsOrigin { get; set; }
    }
    public enum NewsIsCheck
    {
        No = 0,
        Yes = 1
    }
    public enum NewsIsRec
    {
        No = 0,
        Yes = 1
    }
    public enum NewsIsOrigin
    {
        No = 0,
        Yes = 1
    }
}
