﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuoChong.Entity
{
    [Table("Permission")]
    public  class Permission
    {  [Key]
        public int Id { get; set; }
        /// <summary>
        /// 权限id
        /// </summary>
        public int MenuId { get; set; }
        public DateTime CreateTime { get; set; }
        public bool IsDelete { get; set; }
        public int UserId { get; set; }
        /// <summary>
        /// 是否允许访问
        /// </summary>
        public bool IsPass { get; set; }
    }
}
