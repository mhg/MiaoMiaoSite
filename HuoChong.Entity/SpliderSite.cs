﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Entity
{
  public  class SpliderSite
    {
        public int Id { get; set; }
        public string  Url { get; set; }
        public string  Name { get; set; }
        public string  Author { get; set; }
        public DateTime CreateTime { get; set; }
        public SiteType Type { get; set; }
        public bool IsSplider { get; set; }
        public string  Desc { get; set; }
    }
    /// <summary>
    ///  站点类型
    /// </summary>
    public enum SiteType
    {
        /// <summary>
        /// 抓取站点
        /// </summary>
        Splider=0,
        /// <summary>
        /// 前台展示的站点链接
        /// </summary>
        ShowHref=1,
        /// <summary>
        /// 友链
        /// </summary>
        FriendHref=2,
    }
}
