﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HuoChong.Entity
{
    [Table("User")]
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string UserName { get; set; }
        public string IP { get; set; }
        public string Password { get; set; }
        public string Tel { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; } =DateTime.Parse("1990-1-1"); 
        public UserIsDel IsDel { get; set; }
        public int RoleId { get; set; }
    }
    public enum UserIsDel
    {
        No = 0,
        Yes = 1
    }
}
