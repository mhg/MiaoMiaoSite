﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Entity.ViewModel
{
   public class BrowseNumNewsVm
    {
        public  int Id { get; set; }
        public string Content { get; set; }
        public string Title { get; set; }
        public int  Views { get; set; }
        public string Intros { get; set; }
        public int Like { get; set; }
        public string ColumnUrl { get; set; }
    }
}
