﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Entity.ViewModel
{
  public  class ColumnVM
    {
        public int Id { get; set; }
        public string ColumnParentName { get; set; }
        public string ChildName { get; set; }
        public DateTime CreateTime { get; set; }
        public string Spell { get; set; }
    }
}
