﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Entity.ViewModel
{
   public class IndexDataVM
    {
        public List<News> RecNewsList { get; set; }
        public List<NewsPageVM> CategoryNewsList { get; set; }
        public List<BrowseNumNewsVm> BrowserNewsList { get; set; }
        public List<BrowseNumNewsVm> LikeNewsList { get; set; }
        public List<ColumnVM> AllChildColumnList { get; set; }
        /// <summary>
        /// 最新资讯
        /// </summary>
        public List<IndexNewsVM> NewNewsList { get; set; }
        /// <summary>
        /// 猜你喜欢
        /// </summary>
        public List<IndexNewsVM> RamdomLikeList { get; set; }
    }
    
}
