﻿using System;
using System.Collections.Generic;
using System.Text;
namespace HuoChong.Entity.ViewModel
{
  public  class NewsListVM
    {
        public int CurrentPageIndex { get; set; }
        public string CurrentColumnParentName { get; set; }
        public string CurrentColumnParentSpell { get; set; }
        public List<Column> ChildColumnList { get; set; }
        public string Title { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }
    }
}
