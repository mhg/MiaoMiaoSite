﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Entity.ViewModel
{
   public class NewsPageVM
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Source { get; set; }
        public DateTime CreateTime { get; set; }     
        public string Author { get; set; }
        public int ColumnId { get; set; }
        public string  ColumnName { get; set; }
        public string Province { get; set; }
        public int Like { get; set; }
        public int Views { get; set; }
        public NewsIsCheck IsCheck { get; set; }
        public NewsIsRec IsRec { get; set; }
        public string Intros { get; set; }
        public string ColumnUrl { get; set; }
        public bool IsOrigin { get; set; }

    }
}
