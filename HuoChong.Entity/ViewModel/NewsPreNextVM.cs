﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Entity.ViewModel
{
   public class NewsPreNextVM
    {
        public int Id { get; set; }
        public string  Title { get; set; }
    }
    /// <summary>
    /// 相关资讯
    /// </summary>
    public class NewsColumnRealtedVM: NewsPreNextVM
    {
    }
    /// <summary>
    /// 最新资讯
    /// </summary>
    public class IndexNewsVM : NewsPreNextVM
    {
        public string ColumnUrl { get; set; }
    }
}
