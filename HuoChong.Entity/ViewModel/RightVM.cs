﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Entity.ViewModel
{
  public  class RightVM
    {
        public List<BrowseNumNewsVm> ByMonthBrowseNews { get; set; }
        public List<BrowseNumNewsVm> ByMonthLikeNews { get; set; }
        public List<ColumnVM> AllChildColumnByParent { get; set; }
        public List<IndexNewsVM> RamdomLikeList { get; set; }
    }
}
