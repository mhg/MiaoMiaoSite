﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace HuoChong.Tools
{
    public class DESEncrypt
    { 
    public static string DESKey = "mhg0215-20181114";
    #region 加密和解密
   
    /// <summary>
    /// 加密
    /// </summary>
    /// <param name="Text"></param>
    /// <returns></returns>
    public static string Encrypt(string Text)
    {
        return EncryptDES(Text, DESKey);
    }
    /// <summary>  
    /// DES加密字符串  
    /// </summary>  
    /// <param name="encryptString">待加密的字符串</param>  
    /// <param name="encryptKey">加密密钥,要求为16位</param>     
    public static string EncryptDES(string encryptString, string key)
    {
        try
        {
            byte[] rgbKey = Encoding.UTF8.GetBytes(key);
            //用于对称算法的初始化向量（默认值）。  
            byte[] rgbIV = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F };
            byte[] inputByteArray = Encoding.UTF8.GetBytes(encryptString);
            Aes dCSP = Aes.Create();

            MemoryStream mStream = new MemoryStream();
            CryptoStream cStream = new CryptoStream(mStream, dCSP.CreateEncryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
            cStream.Write(inputByteArray, 0, inputByteArray.Length);
            cStream.FlushFinalBlock();
            return Convert.ToBase64String(mStream.ToArray());
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    public static string Decrypt(string Text)
    {
        if (!string.IsNullOrEmpty(Text))
        {
            return DecryptDES(Text, DESKey);
        }
        else
        {
            return "";
        }
    }
    /// <summary>  
    /// DES解密字符串  
    /// </summary>  
    /// <param name="decryptString">待解密的字符串</param>  
    /// <param name="key">解密密钥，要求16位</param>  
    /// <returns></returns>  
    public static string DecryptDES(string decryptString, string key)
    {
        try
        {
            //用于对称算法的初始化向量（默认值）  
            byte[] Keys = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F };
            byte[] rgbKey = Encoding.UTF8.GetBytes(key);
            byte[] rgbIV = Keys;
            byte[] inputByteArray = Convert.FromBase64String(decryptString);
            Aes DCSP = Aes.Create();

            MemoryStream mStream = new MemoryStream();
            CryptoStream cStream = new CryptoStream(mStream, DCSP.CreateDecryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
            cStream.Write(inputByteArray, 0, inputByteArray.Length);
            cStream.FlushFinalBlock();
            return Encoding.UTF8.GetString(mStream.ToArray());
        }
        catch (Exception ex)
        {
            return decryptString;
        }
    }


    #endregion 加密和解密

    #region 计算文件的MD5值
    /// <summary>
    /// 计算文件的MD5值
    /// </summary>
    /// <param name="filepath"></param>
    /// <returns></returns>
    public static String GetStreamMd5(Stream stream)
    {

        string strHashData = "";
        var oMd5Hasher =
            new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] arrbytHashValue = oMd5Hasher.ComputeHash(stream);
        //由以连字符分隔的十六进制对构成的String，其中每一对表示value 中对应的元素；例如“F-2C-4A”
        strHashData = System.BitConverter.ToString(arrbytHashValue);
        //替换-
        strHashData = strHashData.Replace("-", "");

        return strHashData;
    }
        #endregion
    }
}
