﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Tools
{
   public  class MemoryCacheHelper
    {
        private IMemoryCache _cache;
        public MemoryCacheHelper(IMemoryCache cache)
        {
            _cache = cache;
        }
        private static readonly object locker = new object();
    
        public  T GetOrSet<T>(string key,Func<T> func, TimeSpan? slidingExpiration = null) 
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentException(nameof(key));
            if (func == null) throw new ArgumentNullException(nameof(func));
            if (_cache.Get(key)==null)
            {
                lock (locker)
                {
                    if (_cache.Get(key) == null)
                    {
                        _cache.Set(key, func());
                    }
                }
            }
            return _cache.Get<T>(key);
        }
        public void Clear(string key)
        {
            _cache.Remove(key);
        }
    }
}
