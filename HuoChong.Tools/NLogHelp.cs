﻿using Microsoft.Extensions.Logging;
using NLog;
using System;
using System.Collections.Generic;
using System.Text;

namespace HuoChong.Tools
{
    public class NLogHelp
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();
        public static void ErrorLog(string throwMsg, Exception ex)
        {
            string errorMsg = $"【异常信息】：{throwMsg} <br>【异常类型】：{ex.GetType().Name} <br>【堆栈调用】：{ex.StackTrace}";
            errorMsg = errorMsg.Replace("\r\n", "<br>");
            errorMsg = errorMsg.Replace("位置", "<strong style=\"color:red\">位置</strong>");
            logger.Error(errorMsg);
        }
        public static void InfoLog(string operateMsg)
        {
            string errorMsg = string.Format("【操作信息】：{0} <br>",
                new object[] { operateMsg });
            errorMsg = errorMsg.Replace("\r\n", "<br>");
            logger.Info(errorMsg);
        }
    }
}
