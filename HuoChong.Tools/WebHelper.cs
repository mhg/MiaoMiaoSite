﻿
using HuoChong.Entity.AppSettings;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
namespace HuoChong.Tools
{
    public class WebHelper
    {
        private static DomainConfig _domainConfig;
        public WebHelper(IOptionsMonitor<DomainConfig> domainConfig)
        {
            _domainConfig = domainConfig.CurrentValue;
        }
        /// <summary>  
        /// 获取程序集中的实现类对应的多个接口
        /// </summary>  
        /// <param name="assemblyName">程序集</param>
        public static Dictionary<Type, Type[]> GetClassName(string assemblyName)
        {
            if (!String.IsNullOrEmpty(assemblyName))
            {
                Assembly assembly = Assembly.Load(assemblyName);
                List<Type> ts = assembly.GetTypes().ToList();
                var result = new Dictionary<Type, Type[]>();
                foreach (var item in ts.Where(s => !s.IsInterface))
                {
                    var interfaceType = item.GetInterfaces();
                    if (item.IsGenericType) continue;
                    result.Add(item, interfaceType);
                }
                return result;
            }
            return new Dictionary<Type, Type[]>();
        }
        public static string GetContentFirstImg(string content)
        {
            if (!string.IsNullOrEmpty(content))
            {
                string pat = @"<img\b[^<>]*?\bsrc[\s\t\r\n]*=[\s\t\r\n]*[""']?[\s\t\r\n]*(?<imgUrl>[^\s\t\r\n""'<>]*)[^<>]*?/?[\s\t\r\n]*>";
                Regex r = new Regex(pat, RegexOptions.Compiled);
                Match m = r.Match(content);
                if (!m.Success)
                {
                    return "";
                }
                string imgStr = $"{m.Groups[1] + ""}";
                return imgStr;
            }
            else
            {
                return "";
            }
        }
        public static string DealwithContentImg(string content)
        {
            if (!string.IsNullOrEmpty(content))
            {
                string pat = @"<img\b[^<>]*?\bsrc[\s\t\r\n]*=[\s\t\r\n]*[""']?[\s\t\r\n]*(?<imgUrl>[^\s\t\r\n""'<>]*)[^<>]*?/?[\s\t\r\n]*>";              
                MatchCollection mc = Regex.Matches(content,pat);
                if (mc.Count>0)
                {
                    for (int i = 0; i < mc.Count; i++)
                    {
                       string str= mc[i].Groups[1].Value;
                        content = content.Replace($"{mc[i].Groups[1].Value}", $"https://manage.52huochong.com{mc[i].Groups[1].Value}");
                    }
                }                              
                return content;
            }
            else
            {
                return "";
            }
        }
        /// <summary>
        ///Web Client Method ,only For Small picture,else large please use FTP
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="url"></param>
        /// <param name="localPath"></param>
        public static void DownloadUseWebClient(string url, string localPath, out string outTimePath, string webSite = null)
        {
            WebClient wc = new WebClient();
            string fileName = Path.GetFileName(url);
            if (!Directory.Exists(localPath))
            {
                Directory.CreateDirectory(localPath);
            }
            string ymd = DateTime.Now.ToString("yyyyMMdd", DateTimeFormatInfo.InvariantInfo);
            localPath += ymd + "\\";
            if (!Directory.Exists(localPath))
            {
                Directory.CreateDirectory(localPath);
            }
            string path = Path.Combine(localPath, fileName);
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            outTimePath = ymd;
            wc.Headers.Add("Referer", webSite);
            wc.DownloadFile(url, path);
        }
         #region 淘宝api根据ip得到当前城市
        /// <summary>
        /// 淘宝api根据ip得到当前城市
        /// </summary>
        /// <param name = "strIP" ></ param >
        /// < returns ></ returns >
        public static string GetIPCitys(string strIP)
        {
            try
            {
                string Url = "http://ip.taobao.com/service/getIpInfo.php?ip=" + strIP + "";
                System.Net.WebRequest wReq = System.Net.WebRequest.Create(Url);
                wReq.Timeout = 2000;
                System.Net.WebResponse wResp = wReq.GetResponse();
                System.IO.Stream respStream = wResp.GetResponseStream();
                using (System.IO.StreamReader reader = new System.IO.StreamReader(respStream))
                {
                    string jsonText = reader.ReadToEnd();
                    JObject ja = (JObject)JsonConvert.DeserializeObject(jsonText);
                    if (ja["code"].ToString() == "0")
                    {
                        string[] addresArry = { ja["data"]["country"].ToString(), ja["data"]["region"].ToString(), ja["data"]["city"].ToString() };
                        return string.Join("-", addresArry);
                    }
                    else
                    {
                        return "未知";

                    }
                }
            }
            catch (Exception)
            {
                return ("未知");
            }
        }
        #endregion
       

        public static string  GetAddressByIP(string IP)
        {
            try
            {
                string url = "http://whois.pconline.com.cn/ipJson.jsp?callback=testJson&ip=" + IP;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = "text/html;chartset=UTF-8";
                request.UserAgent = "Mozilla / 5.0(Windows NT 10.0; Win64; x64; rv: 48.0) Gecko / 20100101 Firefox / 48.0"; //火狐用户代理
                request.Method = "GET";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                //获取流
                Stream streamResponse = response.GetResponseStream();
                //使用UTF8解码
                using (StreamReader streanReader = new StreamReader(streamResponse, Encoding.GetEncoding("gb2312")))
                {
                    string retString = streanReader.ReadToEnd();

                    string t = retString.Substring(retString.IndexOf("{\""), retString.IndexOf(");}") - retString.IndexOf("{\""));
                    ipinfos m = (ipinfos)JsonConvert.DeserializeObject(t, typeof(ipinfos));

                    string IPProvince = m.pro == "" ? "其它地区" : m.pro;
                    string IPCity = m.city;
                    return $"{IPProvince}-{IPCity}";
                }               
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public class ipinfos
        {
            public string pro { get; set; }
            public string city { get; set; }
        }
        /// <summary>
        /// 移除文本字符的a标签
        /// </summary>    
        public static string ReplaceStrHref(string content)
        {

            var r = new Regex(@"<a\s+(?:(?!</a>).)*?>|</a>", RegexOptions.IgnoreCase);
            Match m;
            for (m = r.Match(content); m.Success; m = m.NextMatch())
            {
                content = content.Replace(m.Groups[0].ToString(), "");
            }
            return content;
        }
        /// <summary>
        /// 移除字符文本Img里面Alt关键字包裹的内链
        /// </summary>      
        public static string RemoveStrImgAlt(string content)
        {
            Regex rg2 = new Regex("(?<=alt=\"<a[^<]*)</a>\"");
            if (rg2.Match(content).Success)
            {
                content = rg2.Replace(content, "");
            }
            Regex rg = new Regex("(?<=alt=\")<a href=\"[^>]*>");
            if (rg.Match(content).Success)
            {
                content = rg.Replace(content, "");
            }
            return content;
        }
    }
}
