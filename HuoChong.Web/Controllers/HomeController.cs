﻿using HuoChong.Data.IRepository;
using HuoChong.Entity;
using HuoChong.Entity.ViewModel;
using HuoChong.Tools;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using QRCoder;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace HuoChong.Web.Controllers
{
    public class HomeController : Controller
    {
        public readonly IQRCode _iQRCode;
        public readonly INewsRepository _newsRepository;
        public readonly IBrowseNumRepository _browseNumRepository;
        public readonly ILikeRepository _likeRepository;
        public readonly IColumnRepository _columnRepository;
        public readonly IMessageRepository _messageRepository;
        public readonly ISpliderSiteRepository _spliderSiteRepository;
        private IMemoryCache _cache;
        public HomeController(INewsRepository newsRepository, IMessageRepository messageRepository, IMemoryCache cache,
        IBrowseNumRepository browseNumRepository, ILikeRepository likeRepository, IColumnRepository columnRepository, ISpliderSiteRepository spliderSiteRepository, IQRCode iQRCode)
        {
            ////IOptions 需要每次重新启动项目加载配置，IOptionsMonitor 每次更改配置都会重新加载，不需要重新启动项目。     
            _newsRepository = newsRepository;
            _browseNumRepository = browseNumRepository;
            _likeRepository = likeRepository;
            _columnRepository = columnRepository;
            _messageRepository = messageRepository;
            _cache = cache;
            _spliderSiteRepository = spliderSiteRepository;
            _iQRCode = iQRCode;
        }
        [Route("/")]
        [Route("/index.html")]
        public async Task<IActionResult> Index()
        {
            IndexDataVM model = new IndexDataVM()
            {
                RecNewsList = (await _newsRepository.LoadIndexRecNews(6)).ToList(),
                CategoryNewsList = (await _newsRepository.LoadCategoryNews()).ToList(),
                BrowserNewsList = (await _newsRepository.GetByBrowseNewsAsync("week")).ToList(),
                LikeNewsList = (await _newsRepository.GetByLikeNewsAsync("week")).ToList(),
                AllChildColumnList = _columnRepository.GetAllChildcolumn(),
                NewNewsList = _newsRepository.GetNewNewsByParentColumn(8, "IT资讯"),
                RamdomLikeList = _newsRepository.GetNewNewsByParentColumn(10, "资源分享")
            };
            if (model == null)
            {
                return NotFound();
            }
            //ViewData["gg"] = _redisSettings.AccessKey;
            return View(model);
        }
        [HttpGet]
        [Route("/{columnUrl}/{id}.html")]
        public async Task<IActionResult> Detail(int id, string columnUrl = null)
        {
            if (id == 0 || string.IsNullOrEmpty(columnUrl))
                return NotFound();
          
           var model=  await _cache.GetOrCreateAsync($"deail-{id}", async x => { return await _newsRepository.NewsDetailAsync(id); });
           
            if (!model.ColumnUrl.Equals(columnUrl))
            {
                return NotFound();
            }
            var upModel = await _newsRepository.LoadPreNextNews(id, model.ColumnUrl, "pre"); ViewBag.UpNews = upModel?.Title;
            ViewBag.UpId = upModel?.Id;

            var nextModel = await _newsRepository.LoadPreNextNews(id, model.ColumnUrl, "next");
            ViewBag.NextNews = nextModel?.Title;
            ViewBag.NextId = nextModel?.Id;
            ViewData["CurrentColumnUrl"] = columnUrl;
            if (model.ColumnId == 0)
            {
                throw new ArgumentNullException(nameof(model.ColumnId));
            }

            ViewData["NewsColumnRelated"] = await _newsRepository.LoadNewsColumnRelated(model.ColumnId, 6);
            await _browseNumRepository.InsertAsync(new BrowseNum
            {
                NewsId = id,
                IP = HttpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault() ?? HttpContext.Connection.RemoteIpAddress.ToString()
            });
            return View(model);
        }
        [HttpPost]
        public async Task<string> ClickLikeNews(int newsId)
        {
            bool i = await _likeRepository.InsertAsync(new Like()
            {
                NewsId = newsId,
                IP = HttpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault() ?? HttpContext.Connection.RemoteIpAddress.ToString(),
                CreateTime = DateTime.Now
            });
            return i ? "ok" : "no";
        }
        [HttpGet]
        [Route("/cate/{columnParentSpell}/page-{page:int}.html")]
        [Route("/cate/{columnParentSpell}/")]
        public async Task<IActionResult> NewsList(string columnParentSpell, int page = 1)
        {
            if (string.IsNullOrEmpty(columnParentSpell))
                return NotFound();
            var allColumn = await _columnRepository.GetListAsync();
           
            var columnParentModel = allColumn.Where(a => a.Spell == columnParentSpell.Trim()).FirstOrDefault();
            if (columnParentModel == null)
            {
                return NotFound();
            }
            NewsListVM model = new NewsListVM()
            {
                CurrentColumnParentSpell = columnParentSpell,
                CurrentColumnParentName = columnParentModel.Name,
                Title = columnParentModel.Title,
                Keywords = columnParentModel.Keywords,
                Description = columnParentModel.Description,
                CurrentPageIndex = page,
                ChildColumnList = allColumn.Where(a => a.ParentId == columnParentModel.Id).ToList()
            };
            var query = await _newsRepository.LoadNewsPage(page, 15, columnParentSpell);
            if (query.Item1.Count < 0)
            {
                throw new ArgumentNullException(nameof(query.Item1));
            }
            ViewData["NewsList"] = query.Item1;
            ViewData["Page"] = new StaticPagedList<NewsPageVM>(query.Item1, page, 15, query.Item2);
            return View(model);
        }
        [HttpGet]
        public IActionResult LoadMessage(int newsId)
        {
            var query = _messageRepository.GetMessageList(newsId);
            if (query.Count > 0)
            {
                NLogHelp.InfoLog($"{DateTime.Now}{query.Count}");
            }
            return Json(new { data = query, count = query.Count });
        }
        [HttpPost]
        public string AddMessage(int newsId, string email, string content)
        {
            try
            {
                Message model = new Message();
                if (!string.IsNullOrEmpty(content) && !string.IsNullOrEmpty(email))
                {
                    model.NewsId = newsId;
                    model.Content = content;
                    model.Email = email;
                    model.IP = HttpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault() ?? HttpContext.Connection.RemoteIpAddress.ToString();
                    model.Address = WebHelper.GetAddressByIP(model?.IP);
                    model.CreateTime = DateTime.Now;
                }
                return _messageRepository.Insert(model) == true ? "ok" : "no";
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        //[Route("/search/{keyword}")]
        public async Task< IActionResult> Search(int page=1,string keyword=null)
        {
            if (string.IsNullOrEmpty(keyword))
            {
                return NotFound();
            }
           var data=await _newsRepository.LoadNewsPage(page, 24, keyword, 1, null, null, null,true);
            ViewData["NewsSearchList"] = data.Item1;
            TempData["CurrentKeyword"] = keyword;
            ViewData["Page"] = new StaticPagedList<NewsPageVM>(data.Item1, page, 24, data.Item2);
            return View();
        }
        [Route("/about.html")]
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }
        [Route("/link.html")]
        public IActionResult Link()
        {
          var linkUrlList=  _spliderSiteRepository.GetList("where Type=1").ToList();
            ViewData["linkUrlList"] = linkUrlList;
            return View();
        }
        [Route("/cate/time")]
        public IActionResult Time()
        {
            return View();
        }

        [Route("/error/404")]
        public IActionResult Error()
        {
            return View();
        }
        /// <summary>
        /// 获取二维码
        /// </summary>
        /// <param name="url">存储内容</param>
        /// <param name="pixel">像素大小</param>
        /// <returns></returns>
        [HttpGet("/api/qrcode")]
        public void GetQRCode(string url, int pixel)
        {
            Response.ContentType = "image/jpeg";
            var bitmap = _iQRCode.GetQRCode(url, pixel);
            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, ImageFormat.Jpeg);

            Response.Body.WriteAsync(ms.GetBuffer(), 0, Convert.ToInt32(ms.Length));
            Response.Body.Close();
        }
        [Route("Barcode.aspx")]
        public IActionResult BarCode()
        {
            return View();
        }
    }
}
