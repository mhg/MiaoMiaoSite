﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HuoChong.WebManage.Services
{
    public class TimerHostedService : BackgroundService
    {
        private readonly ILogger _logger;
        public TimerHostedService(ILogger<TimerHostedService> logger)
        {
            _logger = logger;
        }
        private Timer _timer;
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            //CallBack,一个返回值为void,参数为object的委托,也是计时器执行的方法。

            //state，计时器执行方法的的参数。

            //dueTime,调用 callback 之前延迟的时间量（以毫秒为单位）。

            //period，调用 callback 的时间间隔。
          
            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromHours(3));
            return Task.CompletedTask;
        }

        private void DoWork(object state)//d=> { }
        {
            _logger.LogInformation("Timer is working");
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timer is stopping");
            _timer?.Change(Timeout.Infinite, 0);
            return base.StopAsync(cancellationToken);
        }

        public override void Dispose()
        {
            _timer?.Dispose();
            base.Dispose();
        }
    }
}
