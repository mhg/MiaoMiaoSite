﻿using HuoChong.Data.IRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuoChong.Web.ViewComponents
{
    public class GlobalSearchViewComponent : ViewComponent
    {
        public readonly IColumnRepository _columnRepository;
        public GlobalSearchViewComponent(IColumnRepository columnRepository)
        {
            _columnRepository = columnRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync(int cinemaId)
        {
            return View(await _columnRepository.GetListAsync("where parentid=0"));
            
        }
    }
}
