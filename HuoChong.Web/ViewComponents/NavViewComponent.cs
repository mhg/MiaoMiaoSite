﻿using HuoChong.Data.IRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuoChong.WebManage.ViewComponents
{
    public class NavViewComponent : ViewComponent
    {
        public readonly IColumnRepository _columnRepository;
        public NavViewComponent(IColumnRepository columnRepository)
        {
            _columnRepository = columnRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync(int cinemaId)
        {
         ViewData["AllColumn"]=   await _columnRepository.GetListAsync();
            return View();
        }
    }
}
