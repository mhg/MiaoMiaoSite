﻿using HuoChong.Data.IRepository;
using HuoChong.Entity.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuoChong.WebManage.ViewComponents
{
    public class RightViewComponent: ViewComponent
    {
        public readonly INewsRepository _newsRepository;
        public readonly IColumnRepository _columnRepository;
        public RightViewComponent(INewsRepository newsRepository, IColumnRepository columnRepository)
        {
            _newsRepository = newsRepository;
            _columnRepository = columnRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync(string ColumnParentSpell)
        {
            RightVM model = new RightVM() {
                ByMonthBrowseNews=( await _newsRepository.GetByBrowseNewsAsync("month")).ToList(),
                ByMonthLikeNews=(await _newsRepository.GetByLikeNewsAsync("month")).ToList(),
                AllChildColumnByParent= _columnRepository.GetChildcolumnByParentColumn(ColumnParentSpell),
                RamdomLikeList=_newsRepository.RandomNews()
            };          
            return View(model);
        }
    }
}
