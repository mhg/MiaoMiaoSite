﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuoChong.WebApi.ConfigModel
{
    public class JWTSetting
    {
        public string  ValidIssuser { get; set; }
        public string  ValidAudience { get; set; }
        public string SecretKey { get; set; }
    }
}
