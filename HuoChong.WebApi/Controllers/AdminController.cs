﻿using HuoChong.WebApi.ConfigModel;
using HuoChong.Data.IRepository;
using HuoChong.Entity;
using HuoChong.Tools;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace HuoChong.WebManage.Controllers
{
   // [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : Controller
    {
    
        public readonly IUserRepository _userRepository;
        public readonly ILogRepository _logRepository;
        public readonly JWTSetting _JWTSetting;
        public AdminController(IUserRepository userRepository, ILogRepository logRepository, IOptionsMonitor<JWTSetting> JWTSetting
          )
        {
            _userRepository = userRepository;
            _logRepository = logRepository;
            _JWTSetting = JWTSetting.CurrentValue;
        }
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Login(string ReturnUrl)
        {
            ViewData["ReturnUrl"] = ReturnUrl;
            return View();
        }
        public IActionResult GetToken(string userName, string pwd)
        {
            if (userName != "mhg"&& pwd!="123456")
            {
                return BadRequest();
            }
            var claims = new Claim[] {
                new Claim(ClaimTypes.Name,"mhg"),
                new Claim(ClaimTypes.Role,"admin"),
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_JWTSetting.SecretKey));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                _JWTSetting.ValidIssuser,
                _JWTSetting.ValidAudience,
                claims,
                null,
                DateTime.Now.AddMinutes(30),
                credentials
                );
            return Ok(new { token = new JwtSecurityTokenHandler().WriteToken(token) });
        }

        //[HttpPost, AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> SignIn(string userName, string pwd, string vcode, string ReturnUrl)
        //{
           
        //}
        [AllowAnonymous]
        public FileResult VCode()
        {
            var vcode = VerifyCode.CreateRandomCode(4);
            HttpContext.Session.SetString("vccode", vcode);
            var img = VerifyCode.DrawImage(vcode, 20, Color.White);
            return File(img, "image/gif");
        }
        public IActionResult LogOut()
        {
            HttpContext.SignOutAsync().Wait();
            return RedirectToAction("Login", "Admin");
        }
        public async Task< IActionResult> Register(int id=0)
        {
            if (id > 0)
            {
                var model = await _userRepository.GetModelAsync(id);
                if (model == null)
                {
                    return NotFound();
                }
                ViewData.Model = model;
            }
            else
            {
                ViewData.Model = new User();
            }
            return View();
        }
        [HttpPost]
        public async Task<bool> Register(int id,User input)
        {
            input.IP = HttpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault() ??
                       HttpContext.Connection.RemoteIpAddress.ToString();
            if (id>0)
            {
                input.UpdateTime = DateTime.Now;
                if (!string.IsNullOrEmpty(input.Password))
                {
                    input.Password = DESEncrypt.Encrypt(input.Password);
                    return await _userRepository.UpdateAsync(input) > 0;
                }                
                return await _userRepository.UpdateAsync(input)>0;
            }
            else
            {
                input.Password = DESEncrypt.Encrypt(input.Password);
                input.CreateTime = DateTime.Now;
                input.IsDel = (short)UserIsDel.No;
                return await _userRepository.InsertAsync(input);
            }           
        }       
        public IActionResult Main()
        {
            ViewData["CurrnetUserName"] = User.FindFirst(ClaimTypes.Name)?.Value;
            return View();
        }
        public IActionResult Index()
        {
            ViewData["CurrnetUserName"] = User.FindFirst(ClaimTypes.Name)?.Value;
            return View();
        }
        public IActionResult AdminList()
        {
            return View();
        }
        public IActionResult LoadAdminList(int page, int limit, string key)
        {
            var data = _userRepository.GetListPage(page, limit,$"where isdel={(short)UserIsDel.No}","createtime desc");
            return Json(new { data = data, count =_userRepository.RecordCount($"where isdel={(short)UserIsDel.No}"), code = 0 });
        }
        public bool DelAdmin(string ids)
        {
            if (string.IsNullOrEmpty(ids))
            {
                throw new ArgumentNullException(nameof(ids));
            }
            return _userRepository.MoreDelUser(ids) > 0;
        }
    }
}
