﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HuoChong.Data.IRepository;
using HuoChong.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace HuoChong.WebManage.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ColumnController : Controller
    {
        private readonly IColumnRepository _columnRepository;
        public ColumnController(IColumnRepository columnRepository)
        {
            _columnRepository = columnRepository;
        }
        public IActionResult List()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> GetColumns()
        {
            return Json((await _columnRepository.GetListAsync()).Where(d => d.ParentId == 0).ToList());

        }
        [Route("{parentId}")]
        public async Task<IActionResult> GetChildColumn(int parentId)
        {
            return Ok((await _columnRepository.GetListAsync()).Where(d => d.ParentId == parentId).ToList());
        }
        [HttpGet]
        [Route("/backstage")]
        public async Task<IActionResult> GetAllColumnAsync()
        {
            var data = await _columnRepository.GetListAsync();
            var count = await _columnRepository.RecordCountAsync();
            return Ok(new { data, count, code = 0 });
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Column input)
        {
            if (input == null)
            {
                return NotFound();
            }
            if (await _columnRepository.InsertAsync(input))
            {
                return CreatedAtRoute("GetColumns", new { id = input.Id });
            }
            return BadRequest();
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]Column input)
        {
            if (input == null)
            {
                return BadRequest();
            }
            if (await _columnRepository.UpdateAsync(input) > 0)
            {
                return NoContent();
            }
            return BadRequest();
        }
        //[HttpPatch("{id}")]
        //public async Task<IActionResult> Patch(int id, [FromBody] JsonPatchDocument<Column> input)
        //{
        //    if (input==null)
        //    {
        //        return BadRequest();
        //    }
        //    if (await _columnRepository.UpdateAsync(input))
        //    {
        //        return NoContent();
        //    }            
        //    return BadRequest();
        //}
        [HttpDelete("{ids}")]
        public bool Delete(string ids)
        {
            if (string.IsNullOrEmpty(ids))
            {
                throw new ArgumentNullException(nameof(ids));
            }
            return _columnRepository.Del(ids) > 0 ? true : false;
        }
    }
}