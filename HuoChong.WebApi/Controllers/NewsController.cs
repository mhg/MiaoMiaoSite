﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HuoChong.Data.IRepository;
using HuoChong.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HuoChong.WebManage.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NewsController : Controller
    {
        public readonly INewsRepository _newsRepository;
        public NewsController(INewsRepository newsRepository)
        {
            _newsRepository = newsRepository;
        } 
        public IActionResult List()
        {
            return View();
        }
        public async Task<IActionResult> LoadNewsAsync(int page, int limit, string key, int? rec=null,int?check=(short)NewsIsCheck.No ,int?parentId=null,int?childId=null)
        {
            var data = await _newsRepository.LoadNewsPage(page, limit, key, check, rec,parentId,childId);
            return Json(new { data = data.Item1, count = data.Item2, code = 0 });
        }
   
        [HttpGet]
        public async Task<IActionResult> Edit(int id=0)
        {
            if (id>0)
            {
               var model = await _newsRepository.GetModelAsync(id);
                if (model==null)
                {
                    return NotFound();
                }

                ViewData.Model = model;
            }
            else
            {
                ViewData.Model = new News();
            }
            return View();
        }
        [HttpPost]
        public async Task<bool> Edit(int id,News input)
        {
            if (id>0)
            {
                input.UpdateTime = DateTime.Now;
               
                return await _newsRepository.UpdateNews(input);
            }
            else
            {
                //input.IsCheck = NewsIsCheck.Yes;
                //input.IsRec = NewsIsRec.Yes;
                input.UpdateTime = DateTime.Parse("1990-01-01");
                input.CreateTime=DateTime.Now;
                return await _newsRepository.InsertAsync(input);
            }
        }
        public async Task<bool> Del(string ids)
        {
            if (string.IsNullOrEmpty(ids))
            {
                throw new  ArgumentNullException(nameof(ids));
            }

            return await _newsRepository.DelNews(ids);
        }
        public async Task<bool> IsCheckNews(string ids,bool isCheck)
        {
            if (string.IsNullOrEmpty(ids))
            {
                throw new ArgumentNullException(nameof(ids));
            }
           isCheck= isCheck==true ? false : true;
            return  await _newsRepository.IsCheckNews(ids, isCheck);
        }
        public async Task<bool> IsRecNews(string ids, bool isRec)
        {
            if (string.IsNullOrEmpty(ids))
            {
                throw new ArgumentNullException(nameof(ids));
            }
            isRec = isRec == true ? false : true;
            return await _newsRepository.IsRecNews(ids, isRec);
        }
        public async Task<bool> MoreOperatorNews(string ids, string type,bool bol)
        {
            if (string.IsNullOrEmpty(ids))
            {
                throw new ArgumentNullException(nameof(ids));
            }
            bol = bol == true ? false : true;
            if (type == "check")
            {
                return await _newsRepository.IsCheckNews(ids, bol);
            }
            else {
                return await _newsRepository.IsRecNews(ids, bol);
            }           
        }
    }
}