﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using HuoChong.Data.IRepository;
using HuoChong.Entity;
using HuoChong.Tools;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace HuoChong.WebManage.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SpliderController : Controller
    {
        public readonly INewsRepository _newsRepository;
        public readonly ISpliderSiteRepository _spliderSiteRepository;
        private IHostingEnvironment hostingEnv;
        readonly string uploadFilePath = "editorimage";//保存上传文件的根目录
        public SpliderController(INewsRepository newsRepository, ISpliderSiteRepository spliderSiteRepository,
            IHostingEnvironment hostingEnv)
        {
            _newsRepository = newsRepository;
            _spliderSiteRepository = spliderSiteRepository;
            this.hostingEnv = hostingEnv;
        }
        public IActionResult List()
        {
            return View();
        }
        public IActionResult LoadSiteList(int page, int limit)
        {
            return Json(new { data = _spliderSiteRepository.GetListPage(page, limit), count = _spliderSiteRepository.RecordCount(), code = 0 });
        }
        public IActionResult EditUrl(int id = 0)
        {
            if (id > 0)
            {
                var model = _spliderSiteRepository.GetModel(id);
                if (model == null)
                {
                    return NotFound();
                }
                ViewData.Model = model;
            }
            else
            {
                ViewData.Model = new SpliderSite();
            }
            return View();
        }
        [HttpPost]
        public bool EditUrl(int id, SpliderSite input)
        {
            if (id > 0)
            {
                return _spliderSiteRepository.Update(input) > 0;
            }
            else
            {
                input.CreateTime = DateTime.Now;
                return _spliderSiteRepository.Insert(input);
            }
        }
        public bool DelSite(string ids)
        {
            if (string.IsNullOrEmpty(ids))
            {
                throw new ArgumentNullException(ids);
            }
            return _spliderSiteRepository.MoreDelSite(ids)>0;
        }
        public string LoadWeb(string url, int currentSiteId, string desc)
        {
            if (desc=="博客园")
            {
                return LoadSpliderBusiness(CnBlogContentDetail, url, currentSiteId);
            }
            if (desc== "喷嚏网")
            {
                return LoadSpliderBusiness(LoadDaPenTiWeb, url, currentSiteId);
            }
            return "";
        }
      
        public string LoadSpliderBusiness(Func<string,int,string> func, string url, int currentSiteId)
        {
          return   func.Invoke(url, currentSiteId);
        }
        /// <summary>
        /// 抓取博客园详情页数据
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string CnBlogContentDetail(string url, int currentSiteId)
        {
            try
            {
                HtmlWeb client = new HtmlWeb();
                var doc = client.Load(url);
                if (doc == null)
                    return "no,所加载的url匹配不到document";
                HtmlNode node = doc.DocumentNode.SelectSingleNode("//div[@class='post']");
                if (node == null)
                    return "no,节点匹配不到！";
                var title = node.SelectSingleNode("h1[@class='postTitle']");
                if (title==null)
                    return "no,抓取标题为空";
                HtmlNode node2 = doc.DocumentNode.SelectSingleNode("//div[@class='postBody']");
                if (node2 == null)
                    return "no,所匹配的内容节点为空!";
                var content = node2.SelectSingleNode("div[@id='cnblogs_post_body']");
                if (string.IsNullOrEmpty(content.InnerText))
                {
                    return "no,抓取内容为空!";
                }
                if (!string.IsNullOrEmpty(content.InnerHtml))
                {
                    content.InnerHtml = DealWithContentImg(content);
                }
                return  InsertNews(title.InnerText.Trim(), content.InnerHtml, 4, currentSiteId);
            }
            catch (Exception ex)
            {
                return $"no,{ex.Message}";
            }            
        }
        /// <summary>
        /// 抓取dapenti.com
        /// </summary>     
        public string LoadDaPenTiWeb(string url, int currentSiteId)
        {
            try
            {
                var html = GetHtmlDocument(url);
                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(html);
                if (htmlDocument == null)
                    return $"no,所加载的{url}匹配不到document";
                var htmlnode = htmlDocument.DocumentNode.SelectSingleNode("//table[@class='ke-zeroborder']");
                if (htmlnode == null)
                    return "no,节点匹配不到！";
                var title = htmlnode.SelectSingleNode("//td[@class='oblog_t_4']");//oblog_t_4
                if (title == null)
                    return "no,抓取标题为空！";
                var content = htmlnode.SelectSingleNode("//div[@class='oblog_text']");
                if (string.IsNullOrEmpty(content.InnerText))
                {
                    return "no,抓取内容为空!";
                }
                if (!string.IsNullOrEmpty(content.InnerHtml))
                {
                    content.InnerHtml = DealWithContentImg(content);
                }               
                return InsertNews(title.InnerText.Trim(), content.InnerHtml, 9, currentSiteId);
            }
            catch (Exception ex)
            {
                return $"no,{ex.Message}";
            }           
        }
        /// <summary>
        /// 解决HtmlAgilityPack抓取网页乱码
        /// </summary>      
        private string GetHtmlDocument(string url)
        {
            HttpWebRequest httpWebRequest = WebRequest.Create(new Uri(@url)) as HttpWebRequest;
            httpWebRequest.Method = "GET";
            httpWebRequest.Timeout = 20000;
            httpWebRequest.ContentType = "text/html; charset=gb2312";
            WebResponse webResponse = httpWebRequest.GetResponse();
            Stream stream = webResponse.GetResponseStream();
            using (StreamReader reader = new StreamReader(stream, Encoding.GetEncoding("GB2312")))
            {
                return reader.ReadToEnd();
            }   
        }
        /// <summary>
        /// 文本图片处理
        /// </summary>
        private string  DealWithContentImg(HtmlNode content)
        {
            string pat = @"<img\b[^<>]*?\bsrc[\s\t\r\n]*=[\s\t\r\n]*[""']?[\s\t\r\n]*(?<imgUrl>[^\s\t\r\n""'<>]*)[^<>]*?/?[\s\t\r\n]*>";
            Regex r = new Regex(pat, RegexOptions.Compiled);
            MatchCollection listImg = r.Matches(content.InnerHtml);
            foreach (Match item in listImg)
            {
                if (item.Success)
                {
                    string physicalFilePath = hostingEnv.WebRootPath;
                    string dirPath = physicalFilePath + "\\" + uploadFilePath + "\\" + "image" + "\\";
                    string imgUrl = item.Groups[1].Value.ToString();
                    WebHelper.DownloadUseWebClient(imgUrl, dirPath, out string timePath);
                    string fullImgSrc = "/editorimage/image/" + timePath + imgUrl.Substring(imgUrl.LastIndexOf('/'));
                    content.InnerHtml = content.InnerHtml.Replace(imgUrl, fullImgSrc);
                }
            }
            return content.InnerHtml;
        }
        /// <summary>
        /// 新闻入库
        /// </summary>      
        public string InsertNews(string title, string content,int columnId,int currentSiteId)
        {
            News news = new News()
            {
                Title = title,
                Content = content,
                Source = "网络整理",
                Author = "火虫网",
                Province = "中国",
                CreateTime = DateTime.Now,
                ColumnId = columnId,//变成序列,
                ColumnChildId = 6,//.net core
                UpdateTime = DateTime.Parse("1900-01-01"),
                IsOrigin = NewsIsOrigin.No
            };
            if (_newsRepository.Insert(news))
            {
                _spliderSiteRepository.UpdateSite(currentSiteId);
                return "ok,抓取数据入库成功！";
            }
            return "no,抓取数据入库失败！";
        }
    }
}