﻿using HuoChong.Data.IRepository;
using HuoChong.Entity;
using HuoChong.Tools;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HuoChong.WebManage.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        public readonly IUserRepository _userRepository;
        public readonly ILogRepository _logRepository;
        private readonly IMenuRepository _menuRepository;
        private readonly IPermissionRepository _permissionRepository;
        public AdminController(IUserRepository userRepository, ILogRepository logRepository, IMenuRepository menuRepository,
            IPermissionRepository permissionRepository
          )
        {
            _userRepository = userRepository;
            _logRepository = logRepository;
            _menuRepository = menuRepository;
            _permissionRepository = permissionRepository;
        }
       
        [HttpGet]
        public IActionResult Homepage()
        {
            ViewData["CurrnetUserName"] = User.FindFirst(ClaimTypes.Name)?.Value;
            return View();
        }
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Login(string ReturnUrl)
        {
            ViewData["ReturnUrl"] = ReturnUrl;
            return View();
        }
        [HttpPost, AllowAnonymous]
       // [ValidateAntiForgeryToken]
        public async Task<IActionResult> SignIn(string userName, string pwd, string vcode, string ReturnUrl)
        {
            try
            {
                var session = HttpContext.Session.GetString("vccode") == null ? "" : HttpContext.Session.GetString("vccode").ToString();
                if (string.IsNullOrEmpty(vcode) || session != vcode.ToString())
                {
                    return Content("no,提示：验证码错误！");
                }
                var model = await _userRepository.Login(userName);
                if (model == null)
                {
                    return Content("no,提示：用户名或密码错误");
                }
                string pwd2 = DESEncrypt.Encrypt(pwd);
                if (pwd2.Equals(model.Password))
                {
                    var claims = new[] {
                    new Claim(ClaimTypes.Name,model.UserName),
                    new Claim(ClaimTypes.Sid,model.Id.ToString())};
                    var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                    ClaimsPrincipal user = new ClaimsPrincipal(claimsIdentity);
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, user, new AuthenticationProperties() { IsPersistent = true, ExpiresUtc = DateTime.Now.AddMinutes(200) });
                    string ip = HttpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault() ?? HttpContext.Connection.RemoteIpAddress.ToString();
                    await _logRepository.InsertAsync(
                        new Entity.Log()
                        {
                            UserId = model.Id,
                            CreateTime = DateTime.Now,
                            Content = $"{model.UserName}登录成功,访问地址为{ Request.GetEncodedUrl()}",
                            IP = ip,
                            Address = WebHelper.GetAddressByIP(ip)
                        });
                    if (!string.IsNullOrEmpty(ReturnUrl))
                    {
                        return Redirect(ReturnUrl);
                    }
                    return Content("ok,恭喜：登录成功");
                }
                else
                {
                    return Content("no,提示：密码错误");
                }
            }
            catch (Exception ex)
            {
                NLogHelp.ErrorLog($"登陆失败【{ex.Message}】", ex);             
                return Content("no,提示：登录失败！"+ex+"");                
            }
        }
        [AllowAnonymous]
        public FileResult VCode()
        {
            var vcode = VerifyCode.CreateRandomCode(4);
            HttpContext.Session.SetString("vccode", vcode);
            var img = VerifyCode.DrawImage(vcode, 20, Color.White);
            return File(img, "image/gif");
        }
        public IActionResult LogOut()
        {
            HttpContext.SignOutAsync().Wait();
            return RedirectToAction("Login", "Admin");
        }
        public async Task< IActionResult> Register(int id=0)
        {
            if (id > 0)
            {
                var model = await _userRepository.GetModelAsync(id);
                if (model == null)
                {
                    return NotFound();
                }
                ViewData.Model = model;
            }
            else
            {
                ViewData.Model = new User();
            }
            return View();
        }
        [HttpPost]
        public async Task<bool> Register(int id,User input)
        {
            input.IP = HttpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault() ??
                       HttpContext.Connection.RemoteIpAddress.ToString();
            if (id>0)
            {
                input.UpdateTime = DateTime.Now;
                if (!string.IsNullOrEmpty(input.Password))
                {
                    input.Password = DESEncrypt.Encrypt(input.Password);
                    return await _userRepository.UpdateAsync(input) > 0;
                }                
                return await _userRepository.UpdateAsync(input)>0;
            }
            else
            {
                input.Password = DESEncrypt.Encrypt(input.Password);
                input.CreateTime = DateTime.Now;
                input.IsDel = (short)UserIsDel.No;
                return await _userRepository.InsertAsync(input);
            }           
        }       
        public IActionResult Main()
        {
            ViewData["CurrnetUserName"] = User.FindFirst(ClaimTypes.Name)?.Value;
            return View();
        }
        
        public IActionResult AdminList()
        {
            return View();
        }
        public IActionResult LoadAdminList(int page, int limit, string key)
        {
            var data = _userRepository.GetListPage(page, limit,$"where isdel={(short)UserIsDel.No}","createtime desc");
            return Json(new { data = data, count =_userRepository.RecordCount($"where isdel={(short)UserIsDel.No}"), code = 0 });
        }
        public bool DelAdmin(string ids)
        {
            if (string.IsNullOrEmpty(ids))
            {
                throw new ArgumentNullException(nameof(ids));
            }
            return _userRepository.MoreDelUser(ids) > 0;
        }
        #region 设置权限
        [HttpGet]
        public ActionResult SetMenu(int adminId)
        {
            //  根据id查出当前用户
            var admin = _userRepository.GetModel(adminId);
            //把所有的权限发送到前台
            ViewBag.AllActions = _menuRepository.GetList();
            //把当前用户所有的特殊权限查询出来，发送到前台
            ViewBag.AllExistActions =_permissionRepository.GetList("where userId=@UserId",new { UserId=adminId });
            return View(admin);
        }
        [HttpPost]
        public ActionResult SetMenu(int id, int actionId, string isPass)
        {        
            //在中间表通过用户表id和权限表id查询对应的数据权限
            if (isPass == "true" || isPass == "false")
            {
                Permission temp = _permissionRepository.GetModel(id, actionId);
                bool pass = isPass == "true";
                if (temp != null)
                {
                    // temp.IsPass = pass;
                    _permissionRepository.UpdateTemp(id, actionId, pass);
                }
                else//无则添加
                {
                    Permission p = new Permission() { UserId = id, MenuId = actionId, IsPass = pass };
                    _permissionRepository.Insert(p);
                }
            }
            else
            {//删除
                 _permissionRepository.RealyDelTemp(id, actionId);
            }           
            return Content("ok");
        }
        #endregion
    }
}
