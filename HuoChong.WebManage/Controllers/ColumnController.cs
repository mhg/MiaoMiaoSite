﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HuoChong.Data.IRepository;
using HuoChong.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HuoChong.WebManage.Controllers
{
    [Authorize]
    public class ColumnController : Controller
    {
        private readonly IColumnRepository _columnRepository;
        public ColumnController(IColumnRepository columnRepository)
        {
            _columnRepository = columnRepository;
        }
        public IActionResult List()
        {
            return View();
        }
        public async Task<IActionResult> LoadAllColumn()
        {
            return Json((await _columnRepository.GetListAsync()).Where(d=>d.ParentId==0).ToList());
        }
        public async Task<IActionResult> LoadChildColumn(int parentId)
        {
            return Json((await _columnRepository.GetListAsync()).Where(d=>d.ParentId==parentId).ToList());
        }
        [HttpGet]
        public async Task<IActionResult> LoadAllColumnAsync(int page, int limit, string key)
        {
           var data= await _columnRepository.LoadColumnPageAsync(page,limit,key);          
            return Json(new {  data=data.Item1, count=data.Item2, code = 0 });
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int id = 0)
        {
            if (id > 0)
            {
                var model = await _columnRepository.GetModelAsync(id);
                if (model == null)
                {
                    return NotFound();
                }

                ViewData.Model = model;
            }
            else
            {
                ViewData.Model = new Column();
            }
            return View();
        }
        [HttpPost]
        public async Task<bool> Edit(int id, Column input)
        {
            if (id > 0)
            {
                return await _columnRepository.UpdateAsync(input) > 0;
            }
            else
            {              
                return await _columnRepository.InsertAsync(input);
            }
        }
        [HttpPost]
        public bool Del(string ids)
        {
            if (string.IsNullOrEmpty(ids))
            {
                throw new ArgumentNullException(nameof(ids));
            }
            return _columnRepository.Del(ids) > 0 ? true : false;
        }
    }
}