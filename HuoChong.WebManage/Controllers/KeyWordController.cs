﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HuoChong.Data.IRepository;
using HuoChong.Entity;
using HuoChong.Tools;
using Microsoft.AspNetCore.Mvc;

namespace HuoChong.WebManage.Controllers
{
    public class KeyWordController : Controller
    {
        private readonly IKeyWordRepository _keyWordRepository;
        public KeyWordController(IKeyWordRepository keyWordRepository)
        {
            _keyWordRepository = keyWordRepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult GetKeywordList(int page, int limit, string key=null)
        {
           var data= _keyWordRepository.GetListPage(page,limit,"where keyname=@KeyName","createtime desc",new { KeyName=key });
            var count = _keyWordRepository.RecordCount("where keyname=@KeyName", new { KeyName = key });
            return Json(new { data, count, code = 0 });
        }
        [HttpPost]
        public string Delete(string ids)
        {
            if (string.IsNullOrEmpty(ids))
            {
                throw new ArgumentNullException(nameof(ids));
            }
            return _keyWordRepository.DeleteList("where id in@Ids", new { Ids = ids.Split(',') }) > 0 ? "ok" : "no";
        }
        [HttpGet]
        public IActionResult Edit(int id = 0)
        {
            if (id == 0)
            {
                ViewData.Model = new KeyWord();
            }
            else
            {
                var model = _keyWordRepository.GetModel(id);
                if (model != null)
                {
                    ViewData.Model = model;
                }
                else
                {
                    return NotFound();
                }
            }
            return View();
        }
        [HttpPost]
        public string  Edit(KeyWord model)
        {
           var isExist= _keyWordRepository.GetList($"where keyName={model.KeyName} and id={model.Id}");
            model.UpdateTime = DateTime.Now;
            if (isExist.Count>0)
            {
                return @"ok:关键字已存在";
            }
            if (!string.IsNullOrEmpty(model.Id.ToString()) && model.Id != 0)
            {
               
                if (_keyWordRepository.Update(model) > 0)
                {
                   
                    return @"ok:提交成功";
                }
                return @"no:提交失败";
            }
            else
            {              
                if (_keyWordRepository.Insert(model))
                {                   
                    return @"ok:提交成功";
                }
                return @"no:提交失败";
            }
        }
        [HttpPost]
        public IActionResult ContentReplace(string content, string type)
        {
            try
            {
                if (string.IsNullOrEmpty(content))
                {
                    return Content("请输入文本信息");
                }
                if (type == "edit")
                {
                    content = WebHelper.ReplaceStrHref(content);
                }
                var list = _keyWordRepository.GetList();
                if (list == null)
                {
                    throw new ArgumentNullException(nameof(list));
                }
                #region 第二种办法
                //第一次
                Regex reg = null;
                int n = -1;
                Dictionary<string, string> dic = new Dictionary<string, string>();
                for (int i = 0; i < list.Count; i++)
                {
                    if (Regex.Match(content, list[i].KeyName).Success)
                    {
                        string pattern = $"<a href=\"{list[i].Url}\" target=\"_blank\">{list[i].KeyName}</a>";
                        reg = new Regex(list[i].KeyName);
                        content = reg.Replace(content, pattern, 1);
                        if (Regex.Match(content, pattern).Success)
                        {
                            //如果当前关键字链接信息成功，将当前的文本链接信息提取出来添加到字典中（dic）,并以唯一标识符代替文本链接信息作为占位符。
                            reg = new Regex(pattern);
                            n++;
                            content = reg.Replace(content, "{" + n + "}", 1);
                            dic.Add("{" + n + "}", pattern);
                        }
                    }
                }
                //将匹配到的文本链接信息format
                if (dic.Count != 0)
                {
                    int m = -1;
                    foreach (string key in dic.Keys)
                    {
                        m++;
                        if (content.Contains("{" + m + "}"))
                        {
                            content = content.Replace("{" + m + "}", dic[key]);
                        }
                    }
                }
                #endregion
                // content = keyAddUrl(content, list);
                content = WebHelper.RemoveStrImgAlt(content);
                return Content(content);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}