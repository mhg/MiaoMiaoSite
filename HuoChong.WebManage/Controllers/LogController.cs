﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HuoChong.Data.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HuoChong.WebManage.Controllers
{
    [Authorize]
    public class LogController : Controller
    {
        public readonly ILogRepository _logRepository;                      
        public LogController(ILogRepository logRepository)
        {
            _logRepository = logRepository;
        }
        public IActionResult List()
        {
            return View();
        }
        public IActionResult LoadLogList(int page, int limit, string key)
        {
           var data= _logRepository.LoadLog(page,limit,key);
            return Json(new { data = data.Item1, count =data.Item2 , code = 0 });
        }
        [HttpPost]
        public bool DelLog(string ids)
        {
            if (string.IsNullOrEmpty(ids))
            {
                throw new ArgumentNullException(nameof(ids));
            }
            return _logRepository.MoreDelLog(ids) > 0;
        }
    }
}