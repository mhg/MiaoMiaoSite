﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HuoChong.Data.IRepository;
using HuoChong.Entity;
using Microsoft.AspNetCore.Mvc;

namespace HuoChong.WebManage.Controllers
{
    public class MenuController : Controller
    {
        private readonly IMenuRepository _menuRepository;
        public MenuController(IMenuRepository menuRepository)
        {
            _menuRepository = menuRepository;
        }
        public ActionResult MenuList()
        {
            return View();
        }
        [HttpGet]
        public IActionResult GetMenuList(int page, int limit, string key=null)
        {
            var data = _menuRepository.GetListPage(page, limit, "where menuname=@MenuName", "createtime desc", new { MenuName = key });
            var count = _menuRepository.RecordCount("where MenuName=@MenuName", new { MenuName = key });
            return Json(new { data, count, code = 0 });
           
        }
        #region 添加权限
        [HttpGet]
        public ActionResult InsertMenu(int menuId = 0)
        {
           var data= _menuRepository.GetList();
           
            ViewBag.ParentNameList = data.Where(d=>d.ParentId==0);
            ViewData.Model =data.Where(d=>d.Id==menuId).FirstOrDefault();
            if (ViewData.Model == null)
            {
                ViewData.Model = new Menu();
            }
            return View();
        }

        [HttpPost]      
        public string  InsertMenu(Menu menu)
        {           
            if (!string.IsNullOrEmpty(menu.Id.ToString()) && menu.Id != 0)
            {
                return _menuRepository.Update(menu) > 0 ? "ok" : "no";
            }
            else
            {
                return _menuRepository.Insert(menu) ? "ok" : "no";
            }          
        }
        #endregion    
        #region 删除权限
        public string NoDelMenu(string ids)
        {
            if (string.IsNullOrEmpty(ids))
            {
                throw new ArgumentNullException(nameof(ids));
            }
            return _menuRepository.DeleteList("where id in@Ids", new { Ids = ids.Split(',') }) > 0 ? "ok" : "no";
        }
        #endregion
    }
}