﻿using System;
using HuoChong.Data.ConnectionConfig;
using HuoChong.Tools;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.Cookies;
using Newtonsoft.Json.Serialization;
using System.Text.Encodings.Web;
using System.Text.Unicode;
using Microsoft.Extensions.Logging;
using System.Text;
using NLog.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using System.IO;
using HuoChong.Entity.AppSettings;

namespace HuoChong.WebManage
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            DBConnectionHelper.ConnectionStr = Configuration.GetConnectionString("SqlServerConnection");
            //绑定appsettings
            var host = Configuration.GetSection("FrontDomain");
            services.Configure<DomainConfig>(host);
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            // services.AddHostedService<TimerHostedService>();
            services.AddRouting(options => options.LowercaseUrls = true);
            //解决中文被编码
            services.AddSingleton(HtmlEncoder.Create(UnicodeRanges.All));
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            //集中注入服务
            foreach (var item in WebHelper.GetClassName("HuoChong.Data"))
            {
                foreach (var typeArray in item.Value)
                {
                    services.AddScoped(typeArray, item.Key);
                }
            }
            services.AddResponseCompression();
            services.AddMemoryCache();
            #region XSRF/CSRF
            //services.AddAntiforgery(options =>
            //   {
            //    // Set Cookie properties using CookieBuilder properties†.
            //    options.FormFieldName = "AntiforgeryFieldname";
            //       options.HeaderName = "X-CSRF-TOKEN-huochong";
            //       options.SuppressXFrameOptionsHeader = false;
            //   }); 
            #endregion
            #region cookie使用
            //注册Cookie认证服务【添加授权支持，并添加使用Cookie的方式，配置登录页面和没有权限时的跳转页面。】
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = "/Admin/Login";//设置登录页
                    options.AccessDeniedPath = new PathString("/Error/Forbidden");
                });
            #endregion
            #region session使用
            //在新建Asp.net core应用程序后，要使用session中间件，在startup.cs中需执行三个步骤：
            //1.使用实现了IDistributedcache接口的服务来启用内存缓存。（例如使用内存缓存）
            services.AddDistributedMemoryCache();
            services.AddSession(
                options =>
                {
                    //  options.Cookie.Name = "";
                    options.IdleTimeout = TimeSpan.FromMinutes(20);
                    //设置HttpOnly标志以防止客户端JavaScript中访问Cookie
                    options.Cookie.HttpOnly = true;
                }
                );
            #endregion
          
            services.AddMvc()
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
            .AddJsonOptions(options =>
            {
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
                //不使用驼峰样式的key
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
            });
        }
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {         
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
               
                app.UseExceptionHandler("/error/{0}");
                app.UseHsts();
            }
           
            loggerFactory.AddNLog();
            NLog.LogManager.LoadConfiguration("nlog.config");
            app.UseResponseCompression();
            //静态文件缓存(css,js)
            app.UseStaticFiles(
                new StaticFileOptions
                {                   
                    OnPrepareResponse = ctx =>
                    {
                        const int durationInSeconds = 60 * 60 * 24;
                        ctx.Context.Response.Headers[HeaderNames.CacheControl] =
                            "public,max-age=" + durationInSeconds;
                    }
                });
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseSession();
            app.UseAuthentication();
            //配置状态代码页
            app.UseStatusCodePages(async context =>
            {
                context.HttpContext.Response.ContentType = "text/plain";
                await context.HttpContext.Response.WriteAsync($"抱歉，该信息不存在{context.HttpContext.Response.StatusCode}");
            });
            app.UseStatusCodePagesWithReExecute("/error/{0}");
          
            app.UseMvc(routes =>
            {               
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Admin}/{action=Login}/{id?}");                
            }
            );
           
        }
    }
}
